package com.javainuse.service;

import com.javainuse.dao.ObatDao;
import com.javainuse.dto.ObatDTO;
import com.javainuse.model.DAOObat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtObatDetailsService {

    //    ACTION
    @Autowired
    private ObatDao obatDao;
    private long id;

    //    constructor
    public JwtObatDetailsService() {
    }

    public DAOObat save(ObatDTO obat) {
        DAOObat newObat = new DAOObat();
        newObat.setObat(obat.getObat());
        newObat.setStock(obat.getStock());
        newObat.setExpired(obat.getExpired());

        return obatDao.save(newObat);
    }

    //    for find by id
    public Optional<DAOObat> findById(Long id) {
        return Optional.ofNullable(obatDao.findById(id));
    }

    //    for find all
    public List<DAOObat> findAll(){
        List<DAOObat> obats = new ArrayList<>();
        obatDao.findAll().forEach(obats::add);
        return obats;
    }

    //    for delete
    public void delete(long id) {
        DAOObat obat = obatDao.findById(id);
        obatDao.delete(obat);
    }

    //    for update
    public DAOObat update(Long id) {
        DAOObat obat = obatDao.findById(id);
        return obatDao.save(obat);
    }
}
