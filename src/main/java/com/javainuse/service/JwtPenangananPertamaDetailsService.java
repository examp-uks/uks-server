package com.javainuse.service;

import com.javainuse.dao.PenangananPertamaDao;
import com.javainuse.dto.PenangananPertamaDTO;
import com.javainuse.model.DAOPenangananPertama;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtPenangananPertamaDetailsService {

    //    ACTION
    @Autowired
    private PenangananPertamaDao penangananPertamaDao;
    private long id;

    //    constructor
    public JwtPenangananPertamaDetailsService() {
    }

    public DAOPenangananPertama save(PenangananPertamaDTO penanganan) {
        DAOPenangananPertama newPenyakit = new DAOPenangananPertama();
        newPenyakit.setPenanganan(penanganan.getPenanganan());

        return penangananPertamaDao.save(newPenyakit);
    }

    //    for find by id
    public Optional<DAOPenangananPertama> findById(Long id) {
        return Optional.ofNullable(penangananPertamaDao.findById(id));
    }

    //    for find all
    public List<DAOPenangananPertama> findAll(){
        List<DAOPenangananPertama> penyakits = new ArrayList<>();
        penangananPertamaDao.findAll().forEach(penyakits::add);
        return penyakits;
    }

    //    for delete
    public void delete(long id) {
        DAOPenangananPertama penyakit = penangananPertamaDao.findById(id);
        penangananPertamaDao.delete(penyakit);
    }

    //    for update
    public DAOPenangananPertama update(Long id) {
        DAOPenangananPertama penyakit = penangananPertamaDao.findById(id);
        return penangananPertamaDao.save(penyakit);
    }
}
