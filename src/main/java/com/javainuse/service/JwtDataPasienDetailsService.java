package com.javainuse.service;

import com.javainuse.dao.DataPasienDao;
import com.javainuse.dao.DataSiswaDao;
import com.javainuse.dto.DataPasienDTO;
import com.javainuse.dto.DataSiswaDTO;
import com.javainuse.model.DAODataPasien;
import com.javainuse.model.DAODataSiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtDataPasienDetailsService {

    //    ACTION
    @Autowired
    private DataPasienDao dataPasienDao;
    private long id;

    //    constructor
    public JwtDataPasienDetailsService() {
    }

    public DAODataPasien save(DataPasienDTO dataPasien) {

        DAODataPasien newDataPasien = new DAODataPasien();

        newDataPasien.setStatus(dataPasien.getStatus());
        newDataPasien.setGuru(dataPasien.getGuru());
        newDataPasien.setSiswa(dataPasien.getSiswa());
        newDataPasien.setKaryawan(dataPasien.getKaryawan());
        newDataPasien.setKeluhan(dataPasien.getKeluhan());
        return dataPasienDao.save(newDataPasien);
    }

    //      for find by id
    public Optional<DAODataPasien> findById(long id) {
        return Optional.ofNullable(dataPasienDao.findById(id));
    }

    //    for find all
    public List<DAODataPasien> findAll() {
        List<DAODataPasien> periksas = new ArrayList<>();
        dataPasienDao.findAll().forEach(periksas::add);
        return periksas;
    }

    //    for delete
    public void delete(long id) {
        DAODataPasien dataPasien = dataPasienDao.findById(id);
        dataPasienDao.delete(dataPasien);
    }

    //    for update
    public DAODataPasien update(Long id) {
        DAODataPasien dataPasien = dataPasienDao.findById(id);
        return dataPasienDao.save(dataPasien);
    }

}
