package com.javainuse.service;

import com.javainuse.dao.TindakanDao;
import com.javainuse.dto.TindakanDTO;
import com.javainuse.model.DAOTindakan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtTindakanDetailsService {

    //    ACTION
    @Autowired
    private TindakanDao tindakanDao;
    private long id;

    //    constructor
    public JwtTindakanDetailsService() {
    }

    public DAOTindakan save(TindakanDTO tindakan) {
        DAOTindakan newTindakan = new DAOTindakan();
        newTindakan.setTindakan(tindakan.getTindakan());

        return tindakanDao.save(newTindakan);
    }

    //    for find by id
    public Optional<DAOTindakan> findById(Long id) {
        return Optional.ofNullable(tindakanDao.findById(id));
    }

    //    for find all
    public List<DAOTindakan> findAll(){
        List<DAOTindakan> tindakans = new ArrayList<>();
        tindakanDao.findAll().forEach(tindakans::add);
        return tindakans;
    }

    //    for delete
    public void delete(long id) {
        DAOTindakan tindakan = tindakanDao.findById(id);
        tindakanDao.delete(tindakan);
    }

    //    for update
    public DAOTindakan update(Long id) {
        DAOTindakan tindakan = tindakanDao.findById(id);
        return tindakanDao.save(tindakan);
    }
}
