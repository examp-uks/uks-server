package com.javainuse.service;

import com.javainuse.dao.DataKaryawanDao;
import com.javainuse.dto.DataKaryawanDTO;
import com.javainuse.model.DAODataKaryawan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtDataKaryawanDetailsService {

    //    ACTION
    @Autowired
    private DataKaryawanDao dataKaryawanDao;
    private long id;

    public JwtDataKaryawanDetailsService() {
    }

    //    Constructor
    public DAODataKaryawan save(DataKaryawanDTO dataKaryawan) {
        DAODataKaryawan newDataKaryawan = new DAODataKaryawan();
        newDataKaryawan.setNama(dataKaryawan.getNama());
        newDataKaryawan.setTempat(dataKaryawan.getTempat());
        newDataKaryawan.setTanggal(dataKaryawan.getTanggal());
        newDataKaryawan.setAlamat(dataKaryawan.getAlamat());

        return dataKaryawanDao.save(newDataKaryawan);
    }

    //    for find by id
    public Optional<DAODataKaryawan> findById(Long id) {
        return Optional.ofNullable(dataKaryawanDao.findById(id));
    }

    //    for find all
    public List<DAODataKaryawan> findAll(){
        List<DAODataKaryawan> dataKaryawans = new ArrayList<>();
        dataKaryawanDao.findAll().forEach(dataKaryawans::add);
        return dataKaryawans;
    }

    //    for delete
    public void delete(long id) {
        DAODataKaryawan dataKaryawan = dataKaryawanDao.findById(id);
        dataKaryawanDao.delete(dataKaryawan);
    }

    //    for update
    public DAODataKaryawan update(Long id) {
        DAODataKaryawan dataGuru = dataKaryawanDao.findById(id);
        return dataKaryawanDao.save(dataGuru);
    }
}
