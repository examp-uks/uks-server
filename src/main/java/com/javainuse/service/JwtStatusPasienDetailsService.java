package com.javainuse.service;

import com.javainuse.dao.PenyakitDao;
import com.javainuse.dao.StatusPasienDao;
import com.javainuse.dto.PenyakitDTO;
import com.javainuse.dto.StatusPasienDTO;
import com.javainuse.model.DAOPenyakit;
import com.javainuse.model.DAOStatusPasien;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtStatusPasienDetailsService {

    //    ACTION
    @Autowired
    private StatusPasienDao statusPasienDao;
    private long id;

    //    constructor
    public JwtStatusPasienDetailsService() {
    }

    public DAOStatusPasien save(StatusPasienDTO status) {
        DAOStatusPasien newStatus = new DAOStatusPasien();
        newStatus.setStatus(status.getStatus());

        return statusPasienDao.save(newStatus);
    }

    //    for find by id
    public Optional<DAOStatusPasien> findById(Long id) {
        return Optional.ofNullable(statusPasienDao.findById(id));
    }

    //    for find all
    public List<DAOStatusPasien> findAll(){
        List<DAOStatusPasien> statuses = new ArrayList<>();
        statusPasienDao.findAll().forEach(statuses::add);
        return statuses;
    }

    //    for delete
    public void delete(long id) {
        DAOStatusPasien status = statusPasienDao.findById(id);
        statusPasienDao.delete(status);
    }

    //    for update
    public DAOStatusPasien update(Long id) {
        DAOStatusPasien status = statusPasienDao.findById(id);
        return statusPasienDao.save(status);
    }
}
