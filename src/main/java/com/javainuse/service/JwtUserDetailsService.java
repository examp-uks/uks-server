package com.javainuse.service;

import com.javainuse.dao.UserDao;
import com.javainuse.exception.InternalErrorException;
import com.javainuse.model.DAOTindakan;
import com.javainuse.model.DAOUser;
import com.javainuse.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		DAOUser user = userDao.findByUsername(username);
		List<SimpleGrantedAuthority> roles = null;

		if (user.getRole().equals("admin")) {
			roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
			return new User(username, user.getPassword(), roles);

		}
		if (user.getRole().equals("user")) {
			roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
			return new User(username, user.getPassword(), roles);
		}
		return new User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	public DAOUser save(UserDTO user) {
		DAOUser newUser = new DAOUser();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setRole(user.getRole());
		var password = user.getPassword().trim();
		// digit + lowercase char + uppercase char + punctuation + symbol
		var isPasswordValid = !password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$"
		);
		if(isPasswordValid) throw new InternalErrorException("Password yang anda masukkan tidak valid");
		return userDao.save(newUser);
	}

	//    For find by id
	public Optional<DAOUser> findById(Long id) {
		return Optional.ofNullable(userDao.findById(id));
	}

	//    For update
	public DAOUser update(Long id) {
		DAOUser user = userDao.findById(id);
		return userDao.save(user);
	}
}
