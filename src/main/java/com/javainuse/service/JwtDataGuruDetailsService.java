package com.javainuse.service;

import com.javainuse.dao.DataGuruDao;
import com.javainuse.dto.DataGuruDTO;
import com.javainuse.model.DAODataGuru;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtDataGuruDetailsService {

    //    ACTION
    @Autowired
    private DataGuruDao dataGuruDao;
    private long id;

    //    Constructor
    public JwtDataGuruDetailsService() {
    }

    public DAODataGuru save(DataGuruDTO dataGuru) {
        DAODataGuru newDataGuru = new DAODataGuru();
        newDataGuru.setNama(dataGuru.getNama());
        newDataGuru.setTempat(dataGuru.getTempat());
        newDataGuru.setTanggal(dataGuru.getTanggal());
        newDataGuru.setAlamat(dataGuru.getAlamat());

        return dataGuruDao.save(newDataGuru);
    }

    //    for find by id
    public Optional<DAODataGuru> findById(Long id) {
        return Optional.ofNullable(dataGuruDao.findById(id));
    }

    //    for find all
    public List<DAODataGuru> findAll(){
        List<DAODataGuru> dataGurus = new ArrayList<>();
        dataGuruDao.findAll().forEach(dataGurus::add);
        return dataGurus;
    }

    //    for delete
    public void delete(long id) {
        DAODataGuru dataGuru = dataGuruDao.findById(id);
        dataGuruDao.delete(dataGuru);
    }

    //    for update
    public DAODataGuru update(Long id) {
        DAODataGuru dataGuru = dataGuruDao.findById(id);
        return dataGuruDao.save(dataGuru);
    }
}
