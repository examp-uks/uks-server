package com.javainuse.service;

import com.javainuse.dao.DataSiswaDao;
import com.javainuse.dto.DataSiswaDTO;
import com.javainuse.model.DAODataSiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtDataSiswaDetailsService {

    //    ACTION
    @Autowired
    private  DataSiswaDao dataSiswaDao;
    private long id;

    //    constructor
    public JwtDataSiswaDetailsService() {
    }

    public DAODataSiswa save(DataSiswaDTO dataSiswa) {
        DAODataSiswa newDataSiswa = new DAODataSiswa();
        newDataSiswa.setNama(dataSiswa.getNama());
        newDataSiswa.setTempat(dataSiswa.getTempat());
        newDataSiswa.setTanggal(dataSiswa.getTanggal());
        newDataSiswa.setAlamat(dataSiswa.getAlamat());
        newDataSiswa.setKelas(dataSiswa.getKelas());

        return dataSiswaDao.save(newDataSiswa);
    }

    //      for find by id
    public Optional<DAODataSiswa> findById(long id) {
        return Optional.ofNullable(dataSiswaDao.findById(id));
    }

    //    for find all
    public List<DAODataSiswa> findAll(){
        List<DAODataSiswa> dataSiswas = new ArrayList<>();
        dataSiswaDao.findAll().forEach(dataSiswas::add);
        return dataSiswas;
    }

    //    for delete
    public void delete(long id) {
        DAODataSiswa dataSiswa = dataSiswaDao.findById(id);
        dataSiswaDao.delete(dataSiswa);
    }

    //    for update
    public DAODataSiswa update(Long id) {
        DAODataSiswa dataSiswa = dataSiswaDao.findById(id);
        return dataSiswaDao.save(dataSiswa);
    }
}




