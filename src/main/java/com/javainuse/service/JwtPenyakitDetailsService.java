package com.javainuse.service;

import com.javainuse.dao.PenyakitDao;
import com.javainuse.dto.PenyakitDTO;
import com.javainuse.model.DAOPenyakit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtPenyakitDetailsService {

    //    ACTION
    @Autowired
    private PenyakitDao penyakitDao;
    private long id;

    //    UNTUK POST DATA PENYAKIT
    public JwtPenyakitDetailsService() {
    }

    public DAOPenyakit save(PenyakitDTO penyakit) {
        DAOPenyakit newPenyakit = new DAOPenyakit();
        newPenyakit.setPenyakit(penyakit.getPenyakit());

        return penyakitDao.save(newPenyakit);
    }

    //    for find by id
    public Optional<DAOPenyakit> findById(Long id) {
        return Optional.ofNullable(penyakitDao.findById(id));
    }

    //    for find all
    public List<DAOPenyakit> findAll(){
        List<DAOPenyakit> penyakits = new ArrayList<>();
        penyakitDao.findAll().forEach(penyakits::add);
        return penyakits;
    }

    //    for delete
    public void delete(long id) {
        DAOPenyakit penyakit = penyakitDao.findById(id);
        penyakitDao.delete(penyakit);
    }

    //    for update
    public DAOPenyakit update(Long id) {
        DAOPenyakit penyakit = penyakitDao.findById(id);
        return penyakitDao.save(penyakit);
    }
}
