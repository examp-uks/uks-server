package com.javainuse.service;

import com.javainuse.dao.TanganiDao;
import com.javainuse.dao.TindakanDao;
import com.javainuse.dto.TanganiDTO;
import com.javainuse.dto.TindakanDTO;
import com.javainuse.model.DAOTangani;
import com.javainuse.model.DAOTindakan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtTanganiDetailsService {

    //    ACTION
    @Autowired
    private TanganiDao tanganiDao;
    private long id;

    //    constructor
    public JwtTanganiDetailsService() {
    }

    public DAOTangani save(TanganiDTO tangani) {
        DAOTangani newTindakan = new DAOTangani();
        newTindakan.setPasien(tangani.getPasien());
        newTindakan.setPenyakit(tangani.getPenyakit());
        newTindakan.setPenanganan(tangani.getPenanganan());
        newTindakan.setTindakan(tangani.getTindakan());
        newTindakan.setCatatan(tangani.getCatatan());

        return tanganiDao.save(newTindakan);
    }

    //    for find by id
    public Optional<DAOTangani> findById(Long id) {
        return Optional.ofNullable(tanganiDao.findById(id));
    }

    //    for find all
    public List<DAOTangani> findAll(){
        List<DAOTangani> tanganis = new ArrayList<>();
        tanganiDao.findAll().forEach(tanganis::add);
        return tanganis;
    }

    //    for delete
    public void delete(long id) {
        DAOTangani tangani = tanganiDao.findById(id);
        tanganiDao.delete(tangani);
    }

    //    for update
    public DAOTangani update(Long id) {
        DAOTangani tangani = tanganiDao.findById(id);
        return tanganiDao.save(tangani);
    }
}
