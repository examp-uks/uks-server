package com.javainuse.dto;

public class PenangananPertamaDTO {

    private String  penanganan;

    //    GETTER SETTER
    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }
}
