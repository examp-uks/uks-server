package com.javainuse.dto;

public class DataSiswaDTO {

    private String  nama;

    private String tempat;

    private String tanggal;

    private String alamat;

    private Integer kelas;

    //    GETTER SETTER
    public Integer getKelas() {
        return kelas;
    }

    public void setKelas(Integer kelas) {
        this.kelas = kelas;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
