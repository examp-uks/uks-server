package com.javainuse.dto;

import com.javainuse.model.DAODataPasien;
import com.javainuse.model.DAOPenangananPertama;
import com.javainuse.model.DAOPenyakit;
import com.javainuse.model.DAOTindakan;

public class TanganiDTO {

    private DAODataPasien  pasien;

    private DAOPenyakit penyakit;

    private DAOPenangananPertama penanganan;

    private DAOTindakan tindakan;

    private String catatan;

    //    constructor
//    public TanganiDTO() {
//    }
//
//    public TanganiDTO(DAODataPasien pasien, DAOPenyakit penyakit, DAOPenangananPertama penanganan, DAOTindakan tindakan, String catatan) {
//        this.pasien = pasien;
//        this.penyakit = penyakit;
//        this.penanganan = penanganan;
//        this.tindakan = tindakan;
//        this.catatan = catatan;
//    }

    //    GETTER SETTER
    public DAODataPasien getPasien() {
        return pasien;
    }

    public void setPasien(DAODataPasien pasien) {
        this.pasien = pasien;
    }

    public DAOPenyakit getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(DAOPenyakit penyakit) {
        this.penyakit = penyakit;
    }

    public DAOPenangananPertama getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(DAOPenangananPertama penanganan) {
        this.penanganan = penanganan;
    }

    public DAOTindakan getTindakan() {
        return tindakan;
    }

    public void setTindakan(DAOTindakan tindakan) {
        this.tindakan = tindakan;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }
}
