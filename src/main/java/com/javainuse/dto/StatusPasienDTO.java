package com.javainuse.dto;

public class StatusPasienDTO {

    private String  status;

    //    GETTER SETTER
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
