package com.javainuse.dto;

import com.javainuse.model.DAODataGuru;
import com.javainuse.model.DAODataKaryawan;
import com.javainuse.model.DAODataSiswa;
import com.javainuse.model.DAOStatusPasien;

public class DataPasienDTO {

    private long id;
    private DAOStatusPasien status;

    private DAODataGuru guru;

    private DAODataSiswa siswa;

    private DAODataKaryawan karyawan;

    private String keluhan;

    //    Constructor
//    public DataPasienDTO() {
//
//    }
//
//    public DataPasienDTO(long id, DAOStatusPasien status, DAODataGuru guru, DAODataSiswa siswa, DAODataKaryawan karyawan, String keluhan) {
//        this.id = id;
//        this.status = status;
//        this.guru = guru;
//        this.siswa = siswa;
//        this.karyawan = karyawan;
//        this.keluhan = keluhan;
//    }

    //    getter setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DAOStatusPasien getStatus() {
        return status;
    }

    public void setStatus(DAOStatusPasien status) {
        this.status = status;
    }

    public DAODataGuru getGuru() {
        return guru;
    }

    public void setGuru(DAODataGuru guru) {
        this.guru = guru;
    }

    public DAODataSiswa getSiswa() {
        return siswa;
    }

    public void setSiswa(DAODataSiswa siswa) {
        this.siswa = siswa;
    }

    public DAODataKaryawan getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(DAODataKaryawan karyawan) {
        this.karyawan = karyawan;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }
}
