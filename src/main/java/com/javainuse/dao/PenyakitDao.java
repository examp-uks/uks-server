package com.javainuse.dao;

import com.javainuse.model.DAOPenyakit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PenyakitDao extends CrudRepository<DAOPenyakit, Integer> {

    DAOPenyakit findById(long id);
}
