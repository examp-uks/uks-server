package com.javainuse.dao;

import com.javainuse.model.DAOTangani;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TanganiDao extends CrudRepository<DAOTangani, Integer> {

    DAOTangani findById(long id);
}