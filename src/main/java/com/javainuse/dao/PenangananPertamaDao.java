package com.javainuse.dao;

import com.javainuse.model.DAOPenangananPertama;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenangananPertamaDao extends CrudRepository<DAOPenangananPertama, Integer> {

    DAOPenangananPertama findById(long id);
}