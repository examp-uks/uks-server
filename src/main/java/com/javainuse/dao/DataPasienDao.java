package com.javainuse.dao;

import com.javainuse.model.DAODataPasien;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataPasienDao extends CrudRepository<DAODataPasien, Integer> {

    DAODataPasien findById(long id);
}
