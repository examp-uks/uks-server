package com.javainuse.dao;

import com.javainuse.model.DAODataKaryawan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DataKaryawanDao extends CrudRepository<DAODataKaryawan, Integer> {

    DAODataKaryawan findById(long id);
}
