package com.javainuse.dao;

import com.javainuse.model.DAODataGuru;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DataGuruDao extends CrudRepository<DAODataGuru, Integer> {

    DAODataGuru findById(long id);
}
