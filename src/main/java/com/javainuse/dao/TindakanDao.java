package com.javainuse.dao;

import com.javainuse.model.DAOTindakan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TindakanDao extends CrudRepository<DAOTindakan, Integer> {

    DAOTindakan findById(long id);
}
