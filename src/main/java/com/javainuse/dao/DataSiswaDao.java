package com.javainuse.dao;

import com.javainuse.model.DAODataSiswa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DataSiswaDao extends CrudRepository<DAODataSiswa, Integer> {

    DAODataSiswa findById(long id);
}
