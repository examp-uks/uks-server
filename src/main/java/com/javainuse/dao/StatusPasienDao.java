package com.javainuse.dao;

import com.javainuse.model.DAOStatusPasien;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusPasienDao extends CrudRepository<DAOStatusPasien, Integer> {

    DAOStatusPasien findById(long id);
}