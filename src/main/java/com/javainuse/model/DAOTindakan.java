package com.javainuse.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tindakan")
public class DAOTindakan {

    //    MAKE TABLE TINDAKAN
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @Column
    private String tindakan;

    //    getter setter

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTindakan() {
        return tindakan;
    }

    public void setTindakan(String tindakan) {
        this.tindakan = tindakan;
    }
}
