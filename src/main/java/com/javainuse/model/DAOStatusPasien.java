package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "status")
public class DAOStatusPasien {

    //    MAKE TABLE STATUS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @Column
    private String status;

    //    getter setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
