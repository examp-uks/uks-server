package com.javainuse.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "penyakit")
public class DAOPenyakit {

    //    MAKE TABLE PENYAKIT
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @Column
    private String penyakit;


    //    getter setter

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(String penyakit) {
        this.penyakit = penyakit;
    }
}
