package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "siswa")
public class DAODataSiswa {

    //    MAKE TABLE SISWA
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @Column
    private String nama;
    @Column
    private String tempat;
    @Column
    private String tanggal;
    @Column
    private String alamat;
    @Column
    private Integer kelas;

    //    getter setter

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Integer getKelas() {
        return kelas;
    }

    public void setKelas(Integer kelas) {
        this.kelas = kelas;
    }
}
