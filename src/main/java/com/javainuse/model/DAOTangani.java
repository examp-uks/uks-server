package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "tangani")
public class DAOTangani {

    //    MAKE TABLE STATUS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @ManyToOne
    private DAODataPasien pasien;

    @ManyToOne
    private DAOPenyakit penyakit;

    @ManyToOne
    private DAOPenangananPertama penanganan;

    @ManyToOne
    private DAOTindakan tindakan;

    @Column
    private String catatan;

    //    Constructor
    public DAOTangani() {
    }

    public DAOTangani(long id, DAODataPasien pasien, DAOPenyakit penyakit, DAOPenangananPertama penanganan, DAOTindakan tindakan, String catatan) {
        this.id = id;
        this.pasien = pasien;
        this.penyakit = penyakit;
        this.penanganan = penanganan;
        this.tindakan = tindakan;
        this.catatan = catatan;
    }

//    getter setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DAODataPasien getPasien() {
        return pasien;
    }

    public void setPasien(DAODataPasien pasien) {
        this.pasien = pasien;
    }

    public DAOPenyakit getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(DAOPenyakit penyakit) {
        this.penyakit = penyakit;
    }

    public DAOPenangananPertama getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(DAOPenangananPertama penanganan) {
        this.penanganan = penanganan;
    }

    public DAOTindakan getTindakan() {
        return tindakan;
    }

    public void setTindakan(DAOTindakan tindakan) {
        this.tindakan = tindakan;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }
}
