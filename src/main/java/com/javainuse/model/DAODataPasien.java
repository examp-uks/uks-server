package com.javainuse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;


@Entity
@Table(name = "pasien")
public class DAODataPasien {

    //    Make table pasien
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private DAOStatusPasien status;

    @ManyToOne
    private DAODataGuru guru;

    @ManyToOne
    private DAODataSiswa siswa;

    @ManyToOne
    private DAODataKaryawan karyawan;

    @Column
    private String keluhan;

    //    Constructor
    public DAODataPasien() {
    }

    public DAODataPasien(long id, DAOStatusPasien status, DAODataGuru guru, DAODataSiswa siswa, DAODataKaryawan karyawan, String keluhan) {
        this.id = id;
        this.status = status;
        this.guru = guru;
        this.siswa = siswa;
        this.karyawan = karyawan;
        this.keluhan = keluhan;
    }

    //    getter setter
    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setStatus(DAOStatusPasien status) {
        this.status = status;
    }

    public DAODataGuru getGuru() {
        return guru;
    }

    public void setGuru(DAODataGuru guru) {
        this.guru = guru;
    }

    public DAODataSiswa getSiswa() {
        return siswa;
    }
    public void setSiswa(DAODataSiswa siswa) {
        this.siswa = siswa;
    }

    public DAOStatusPasien getStatus() {
        return status;
    }

    public DAODataKaryawan getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(DAODataKaryawan karyawan) {
        this.karyawan = karyawan;
    }

}
