package com.javainuse.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "penanganan")
public class DAOPenangananPertama {

    //    MAKE TABLE PENANGANAN
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    @Column
    private String penanganan;

    //    getter setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }
}
