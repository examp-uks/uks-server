package com.javainuse.controller;

import com.javainuse.dto.ObatDTO;
import com.javainuse.dto.PenyakitDTO;
import com.javainuse.model.DAOObat;
import com.javainuse.model.DAOPenyakit;
import com.javainuse.service.JwtObatDetailsService;
import com.javainuse.service.JwtPenyakitDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/obat")
@CrossOrigin(origins = "http://localhost:3000")
public class ObatController {

    //    SAVE METHOD
    public static final Logger logger = LoggerFactory.getLogger(ObatController.class);

    @Autowired
    private JwtObatDetailsService obatDetailsService;

    // ---------------------------------Create a data obat-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createData(@RequestBody ObatDTO obat) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", obat);

        obatDetailsService.save(obat);

        return new ResponseEntity<>(obat, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data obat-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOObat>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAOObat> obats = obatDetailsService.findAll();

        return new ResponseEntity<>(obats, HttpStatus.OK);
    }

    // ---------------------------------Get Single data obat-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data with id {}", id);

        Optional<DAOObat> obat = obatDetailsService.findById(id);

        if (obat == null) {
            logger.error("data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(obat, HttpStatus.OK);
    }

    // ---------------------------------Update data obat-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody ObatDTO obat) throws SQLException, ClassNotFoundException {
        logger.info("Updating data with id {}", id);

        Optional<DAOObat> currentData = obatDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setObat(obat.getObat());
        currentData.orElseThrow().setStock(obat.getStock());
        currentData.orElseThrow().setExpired(obat.getExpired());

        obatDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data obat-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data with id {}", id);

        obatDetailsService.delete(id);
        return new ResponseEntity<DAOObat>(HttpStatus.NO_CONTENT);
    }
}
