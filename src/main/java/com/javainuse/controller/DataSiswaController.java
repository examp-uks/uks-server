package com.javainuse.controller;


import com.javainuse.dto.DataSiswaDTO;
import com.javainuse.model.DAODataSiswa;
import com.javainuse.service.JwtDataSiswaDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/siswa")
@CrossOrigin(origins = "http://localhost:3000")
public class DataSiswaController {

    public static final Logger logger = LoggerFactory.getLogger(DataSiswaController.class);

    @Autowired
    private JwtDataSiswaDetailsService dataSiswaDetailsService;

    // ---------------------------------Create a data siswa-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody DataSiswaDTO dataSiswa) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", dataSiswa);

        dataSiswaDetailsService.save(dataSiswa);

        return new ResponseEntity<>(dataSiswa, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data siswa-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAODataSiswa>> listAllDataGuru() throws SQLException, ClassNotFoundException {

        List<DAODataSiswa> dataSiswas = dataSiswaDetailsService.findAll();

        return new ResponseEntity<>(dataSiswas, HttpStatus.OK);
    }

    // ---------------------------------Get Single data siswa-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data a with id {}", id);

        Optional<DAODataSiswa> dataSiswa = dataSiswaDetailsService.findById(id);

        if (dataSiswa == null) {
            logger.error("data siswa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data guru with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dataSiswa, HttpStatus.OK);
    }

    // ---------------------------------Update data siswa-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody DataSiswaDTO dataSiswa) throws SQLException, ClassNotFoundException {
        logger.info("Updating data siswa with id {}", id);

        Optional<DAODataSiswa> currentData = dataSiswaDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data siswa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data siswa with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setNama(dataSiswa.getNama());
        currentData.orElseThrow().setTempat(dataSiswa.getTempat());
        currentData.orElseThrow().setTanggal(dataSiswa.getTanggal());
        currentData.orElseThrow().setAlamat(dataSiswa.getAlamat());
        currentData.orElseThrow().setKelas(dataSiswa.getKelas());

        dataSiswaDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data siswa-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data siswa with id {}", id);

        dataSiswaDetailsService.delete(id);
        return new ResponseEntity<DAODataSiswa>(HttpStatus.NO_CONTENT);
    }
}