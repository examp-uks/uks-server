package com.javainuse.controller;

import com.javainuse.dto.TanganiDTO;
import com.javainuse.model.DAOTangani;
import com.javainuse.service.JwtTanganiDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tangani")
@CrossOrigin(origins = "http://localhost:3000")
public class TanganiController {

    //    SAVE METHOD
    public static final Logger logger = LoggerFactory.getLogger(TanganiController.class);

    @Autowired
    private JwtTanganiDetailsService tanganiDetailsService;

    // ---------------------------------Create a data tangani-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createData(@RequestBody TanganiDTO tangani) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", tangani);

        tanganiDetailsService.save(tangani);

        return new ResponseEntity<>(tangani, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data tangani-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOTangani>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAOTangani> tanganis = tanganiDetailsService.findAll();

        return new ResponseEntity<>(tanganis, HttpStatus.OK);
    }

    // ---------------------------------Get Single data tindakan-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data with id {}", id);

        Optional<DAOTangani> tindakan = tanganiDetailsService.findById(id);

        if (tindakan == null) {
            logger.error("data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(tindakan, HttpStatus.OK);
    }

    // ---------------------------------Update data tangani-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody TanganiDTO tindakan) throws SQLException, ClassNotFoundException {
        logger.info("Updating data with id {}", id);

        Optional<DAOTangani> currentData = tanganiDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setPasien(tindakan.getPasien());
        currentData.orElseThrow().setPenyakit(tindakan.getPenyakit());
        currentData.orElseThrow().setPenanganan(tindakan.getPenanganan());
        currentData.orElseThrow().setTindakan(tindakan.getTindakan());
        currentData.orElseThrow().setCatatan(tindakan.getCatatan());

        tanganiDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data tangani-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data with id {}", id);

        tanganiDetailsService.delete(id);
        return new ResponseEntity<DAOTangani>(HttpStatus.NO_CONTENT);
    }
}
