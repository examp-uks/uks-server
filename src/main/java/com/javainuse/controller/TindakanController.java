package com.javainuse.controller;

import com.javainuse.dto.TindakanDTO;
import com.javainuse.model.DAOTindakan;
import com.javainuse.service.JwtTindakanDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tindakan")
@CrossOrigin(origins = "http://localhost:3000")
public class TindakanController {

    //    SAVE METHOD
    public static final Logger logger = LoggerFactory.getLogger(PenyakitController.class);

    @Autowired
    private JwtTindakanDetailsService tindakanDetailsService;

    // ---------------------------------Create a data tindakan-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createData(@RequestBody TindakanDTO tindakan) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", tindakan);

        tindakanDetailsService.save(tindakan);

        return new ResponseEntity<>(tindakan, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data tindakan-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOTindakan>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAOTindakan> penyakits = tindakanDetailsService.findAll();

        return new ResponseEntity<>(penyakits, HttpStatus.OK);
    }

    // ---------------------------------Get Single data tindakan-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data with id {}", id);

        Optional<DAOTindakan> tindakan = tindakanDetailsService.findById(id);

        if (tindakan == null) {
            logger.error("data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(tindakan, HttpStatus.OK);
    }

    // ---------------------------------Update data tindakan-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody TindakanDTO tindakan) throws SQLException, ClassNotFoundException {
        logger.info("Updating data with id {}", id);

        Optional<DAOTindakan> currentData = tindakanDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setTindakan(tindakan.getTindakan());

        tindakanDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data tindakan-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data with id {}", id);

        tindakanDetailsService.delete(id);
        return new ResponseEntity<DAOTindakan>(HttpStatus.NO_CONTENT);
    }
}
