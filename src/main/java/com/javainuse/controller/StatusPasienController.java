package com.javainuse.controller;

import com.javainuse.dto.PenyakitDTO;
import com.javainuse.dto.StatusPasienDTO;
import com.javainuse.model.DAOPenyakit;
import com.javainuse.model.DAOStatusPasien;
import com.javainuse.service.JwtPenyakitDetailsService;
import com.javainuse.service.JwtStatusPasienDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/status")
@CrossOrigin(origins = "http://localhost:3000")
public class StatusPasienController {

    //    SAVE METHOD
    public static final Logger logger = LoggerFactory.getLogger(StatusPasienController.class);

    @Autowired
    private JwtStatusPasienDetailsService statusPasienDetailsService;

    // ---------------------------------Create a data penyakit-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createData(@RequestBody StatusPasienDTO status) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", status);

        statusPasienDetailsService.save(status);

        return new ResponseEntity<>(status, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data penyakit-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOStatusPasien>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAOStatusPasien> statuses = statusPasienDetailsService.findAll();

        return new ResponseEntity<>(statuses, HttpStatus.OK);
    }

    // ---------------------------------Get Single data penyakit-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data with id {}", id);

        Optional<DAOStatusPasien> data = statusPasienDetailsService.findById(id);

        if (data == null) {
            logger.error("data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    // ---------------------------------Update data penyakit-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody StatusPasienDTO status) throws SQLException, ClassNotFoundException {
        logger.info("Updating data with id {}", id);

        Optional<DAOStatusPasien> currentData = statusPasienDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data guru with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setStatus(status.getStatus());

        statusPasienDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data penyakit-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data with id {}", id);

        statusPasienDetailsService.delete(id);
        return new ResponseEntity<DAOStatusPasien>(HttpStatus.NO_CONTENT);
    }
}
