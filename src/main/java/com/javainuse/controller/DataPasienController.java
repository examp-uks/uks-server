package com.javainuse.controller;

import com.javainuse.dto.DataPasienDTO;
import com.javainuse.dto.DataSiswaDTO;
import com.javainuse.model.DAODataPasien;
import com.javainuse.model.DAODataSiswa;
import com.javainuse.service.JwtDataPasienDetailsService;
import com.javainuse.service.JwtDataSiswaDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pasien")
@CrossOrigin(origins = "http://localhost:3000")
public class DataPasienController {

    public static final Logger logger = LoggerFactory.getLogger(DataPasienController.class);

    @Autowired
    private JwtDataPasienDetailsService dataPasienDetailsService;

    // ---------------------------------Create a data pasien-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody DataPasienDTO dataPasien) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", dataPasien);

        dataPasienDetailsService.save(dataPasien);

        return new ResponseEntity<>(dataPasien, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data pasien-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAODataPasien>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAODataPasien> dataPasiens = dataPasienDetailsService.findAll();

        return new ResponseEntity<>(dataPasiens, HttpStatus.OK);
    }

    // ---------------------------------Get Single data pasien-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data a with id {}", id);

        Optional<DAODataPasien> dataPasien = dataPasienDetailsService.findById(id);

        if (dataPasien == null) {
            logger.error("data siswa with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data guru with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dataPasien, HttpStatus.OK);
    }

    // ---------------------------------Update data pasien-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody DataPasienDTO dataPasien) throws SQLException, ClassNotFoundException {
        logger.info("Updating data with id {}", id);

        Optional<DAODataPasien> currentData = dataPasienDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setStatus(dataPasien.getStatus());
        currentData.orElseThrow().setGuru(dataPasien.getGuru());
        currentData.orElseThrow().setSiswa(dataPasien.getSiswa());
        currentData.orElseThrow().setKaryawan(dataPasien.getKaryawan());
        currentData.orElseThrow().setKeluhan(dataPasien.getKeluhan());

        dataPasienDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data pasien-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data with id {}", id);

        dataPasienDetailsService.delete(id);
        return new ResponseEntity<DAODataPasien>(HttpStatus.NO_CONTENT);
    }
}
