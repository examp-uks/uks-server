package com.javainuse.controller;

import com.javainuse.dto.DataKaryawanDTO;
import com.javainuse.model.DAODataKaryawan;
import com.javainuse.service.JwtDataKaryawanDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/karyawan")
@CrossOrigin(origins = "http://localhost:3000")
public class DataKaryawanController {

    //    SAVE METHOD

    public static final Logger logger = LoggerFactory.getLogger(DataGuruController.class);

    @Autowired
    private JwtDataKaryawanDetailsService dataKaryawanDetailsService;

    // ---------------------------------Create a data karyawan-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createData(@RequestBody DataKaryawanDTO dataKaryawan) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", dataKaryawan);

        dataKaryawanDetailsService.save(dataKaryawan);

        return new ResponseEntity<>(dataKaryawan, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data karyawan-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAODataKaryawan>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAODataKaryawan> dataKaryawans = dataKaryawanDetailsService.findAll();

        return new ResponseEntity<>(dataKaryawans, HttpStatus.OK);
    }

    // ---------------------------------Get Single data karyawan-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data karyawan with id {}", id);

        Optional<DAODataKaryawan> dataKaryawan = dataKaryawanDetailsService.findById(id);

        if (dataKaryawan == null) {
            logger.error("data karyawan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data karyawan with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dataKaryawan, HttpStatus.OK);
    }

    // ---------------------------------Update data karyawan-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody DataKaryawanDTO dataKaryawan) throws SQLException, ClassNotFoundException {
        logger.info("Updating data karyawan with id {}", id);

        Optional<DAODataKaryawan> currentData = dataKaryawanDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data karyawan with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data karyawan with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setNama(dataKaryawan.getNama());
        currentData.orElseThrow().setTempat(dataKaryawan.getTempat());
        currentData.orElseThrow().setTanggal(dataKaryawan.getTanggal());
        currentData.orElseThrow().setAlamat(dataKaryawan.getAlamat());

        dataKaryawanDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data karyawan-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data karyawan with id {}", id);

        dataKaryawanDetailsService.delete(id);
        return new ResponseEntity<DAODataKaryawan>(HttpStatus.NO_CONTENT);
    }
}
