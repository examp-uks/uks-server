package com.javainuse.controller;

import com.javainuse.dto.PenangananPertamaDTO;
import com.javainuse.model.DAOPenangananPertama;
import com.javainuse.service.JwtPenangananPertamaDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/penanganan")
@CrossOrigin(origins = "http://localhost:3000")
public class PenangananPertamaController {

    //    SAVE METHOD
    public static final Logger logger = LoggerFactory.getLogger(PenangananPertamaController.class);

    @Autowired
    private JwtPenangananPertamaDetailsService penangananPertamaDetailsService;

    // ---------------------------------Create a data penanganan-------------------------------------------
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createData(@RequestBody PenangananPertamaDTO penangananPertama) throws SQLException, ClassNotFoundException {
        logger.info("Creating Data : {}", penangananPertama);

        penangananPertamaDetailsService.save(penangananPertama);

        return new ResponseEntity<>(penangananPertama, HttpStatus.CREATED);
    }

    // ---------------------------------Get All data penanganan-------------------------------------------
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOPenangananPertama>> listAllData() throws SQLException, ClassNotFoundException {

        List<DAOPenangananPertama> penangananPertamas = penangananPertamaDetailsService.findAll();

        return new ResponseEntity<>(penangananPertamas, HttpStatus.OK);
    }

    // ---------------------------------Get Single data penanganan-------------------------------------------
    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching data with id {}", id);

        Optional<DAOPenangananPertama> dataPenanganan = penangananPertamaDetailsService.findById(id);

        if (dataPenanganan == null) {
            logger.error("data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("data with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dataPenanganan, HttpStatus.OK);
    }

    // ---------------------------------Update data penanganan-------------------------------------------
    @RequestMapping(value = "/data/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateData(@PathVariable("id") long id, @RequestBody PenangananPertamaDTO penanganan) throws SQLException, ClassNotFoundException {
        logger.info("Updating data with id {}", id);

        Optional<DAOPenangananPertama> currentData = penangananPertamaDetailsService.findById(id);

        if (currentData == null) {
            logger.error("Unable to update. data with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. data with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentData.orElseThrow().setPenanganan(penanganan.getPenanganan());

        penangananPertamaDetailsService.update(currentData.get().getId());
        return new ResponseEntity<>(currentData, HttpStatus.OK);
    }

    // ---------------------------------Delete data penanganan-------------------------------------------
    @RequestMapping(value = "/data/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteData(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting data with id {}", id);

        penangananPertamaDetailsService.delete(id);
        return new ResponseEntity<DAOPenangananPertama>(HttpStatus.NO_CONTENT);
    }
}
